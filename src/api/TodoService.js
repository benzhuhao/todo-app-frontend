import axios from 'axios';
import TODO_API_URL from 'Constants';

class TodoService {

    retrieveAllTodos(username) {
        return axios.get(`${TODO_API_URL}/all/${username}`);
    }

    retrieveTodo(username, id) {
        return axios.get(`${TODO_API_URL}/${id}/${username}`);
    }

    createTodo(todo) {
        return axios.post(`/save`, todo);
    }

    updateTodo(todo) {
        return axios.put(`/update`, todo);
    }

    deleteTodo(id) {
        return axios.delete(`/delete/${id}`);
    }
}

export default new TodoService()