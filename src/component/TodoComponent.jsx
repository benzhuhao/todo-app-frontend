import React, { Component } from 'react';
import { Formik, Form, Field, ErrorMessage } from 'formik';
import moment from 'moment';
import TodoService from '../api/TodoService'
import AuthenticationService from '../api/AuthenticationService'
import classNames from "classnames";

class TodoComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: this.props.match.params.id,
            username: '',
            description: '',
            lastUpdate: '',
            isDone: false
        }
    }

    componentDidMount() {
        if(this.state.id === -1) {
            return;
        }
        let username = AuthenticationService.getLoggedInUserName();

        TodoService.retrieveTodo(username, this.state.id)
            .then( resp => this.setState({
                username: username,
                description: resp.data.description,
                lastUpdate: resp.data.lastUpdate,
                isDone: resp.data.isDone
            }))
    }

    validate(values) {
        let errors = {};
        if( !values.description ) {
            errors.description = 'Enter a Description';
        } else if( values.description.length < 5 ) {
            errors.description = 'Enter at least 5 Characters in Description';
        }

        return errors;
    }

    onSubmit(values) {
        let username = AuthenticationService.getLoggedInUserName();

        let todo = {
            id: this.state.id,
            username: username,
            description: values.description,
            lastUpdate: moment(new Date()).format('YYY-MM-DDThh:mm:ss'),
            isDone: values.isDone
        }

        if( this.state.id === -1 ) {
            TodoService.createTodo(todo)
                .then(
                    () => this.props.history.push('/todos')
                )
        } else {
            TodoService.updateTodo(todo)
                .then(
                    () => this.props.history.push('/todos')
                )
        }
    }
    
    Checkbox({
        field: { name, value, onChange, onBlur },
        id,
        label,
        className,
        ...props
      }) {
        return (
          <div>
            <input
              name={name}
              id={id}
              type="checkbox"
              value={value}
              checked={value}
              onChange={onChange}
              onBlur={onBlur}
              className={classNames("radio-button")}
            />
            <label htmlFor={id}>{label}</label>
          </div>
        );
    };

    render() {

        let { description, isDone } = this.state

        return (
            <div>
                <h1>Todo</h1>
                <div className="container">
                    <Formik
                        initialValues={{ description, isDone }}
                        onSubmit={this.onSubmit}
                        validateOnChange={false}
                        validateOnBlur={false}
                        validate={this.validate}
                        enableReinitialize={true}
                    >
                        {
                            (props) => (
                                <Form>
                                    <ErrorMessage name="description" component="div"
                                        className="alert alert-warning" />
                                    <fieldset className="form-group">
                                        <label>Description</label>
                                        <Field className="form-control" type="text" name="description" />
                                    </fieldset>
                                    <Field
                                        component={Checkbox}
                                        name="singleCheckbox"
                                        id="singleCheckbox"
                                        label="checked"
                                    />
                                    <button className="btn btn-success" type="submit">Save</button>
                                </Form>
                            )
                        }
                    </Formik>
                </div>
            </div>
        );
    }
}

export default TodoComponent;